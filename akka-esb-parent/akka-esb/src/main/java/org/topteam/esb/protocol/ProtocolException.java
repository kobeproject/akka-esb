package org.topteam.esb.protocol;

import org.topteam.esb.exception.AkkaEsbException;

public class ProtocolException extends AkkaEsbException{

	private static final long serialVersionUID = -3659791269435091639L;

	public ProtocolException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProtocolException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ProtocolException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ProtocolException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
