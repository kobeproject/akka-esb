package org.topteam.esb.http;

import org.topteam.esb.common.URL;


public interface HttpServer {

	void start();
	
	void stop();
	
	URL getUrl();
	
	Handler getHandler();
}
